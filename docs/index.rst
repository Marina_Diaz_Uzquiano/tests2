# Search Genes
-----------------------

First, enter your gene(s) of interest to get their expression values

![Search genes by](img/SearchGenes_A.png)

*  Your genes can be idenitified by: locus ID, gene name, gene symbol, or ITAG description

* If none of these identifiers are available to you but you have your gene's sequence, we provided here the link to Sol Genomics Blast so you can get your gene's locus ID.

------------------------------------------

![Line Types](img/SearchGenes_B.png) 

* You can also filter your results by phenotype (wild type / mutant)

------------------------------------------

![Advanced Search](img/SearchGenes_C.png)



* On top of that you have an advanced search where you can filter your expression results by organ, treatment, and cultivar


# Genes Search Output

Click search to see the output!

You'll get a table with all genes you've requested.
